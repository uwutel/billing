from typing import Annotated

from fastapi import FastAPI, Form
from pydantic import BaseModel

import json
import os

DATA_DIR = "/data" if "DATA_DIR" not in os.environ else os.environ["DATA_DIR"]

app = FastAPI()

if not os.path.exists(f"{DATA_DIR}/ext.json"):
    with open(f"{DATA_DIR}/ext.json", "w") as ext_file:
        json.dump({}, ext_file)

ext_list = json.load(open(f"{DATA_DIR}/ext.json"))

@app.get("/")
def read_root():
    return ext_list

@app.get("/ext/{ext_id}")
def read_ext(ext_id: str):
    return ext_list[ext_id] if ext_id in ext_list else -1

@app.post("/ext/{ext_id}")
def set_ext(ext_id: str, credits: Annotated[int, Form()]):
    if ext_id in ext_list:
        ext_list[ext_id] = credits if ext_list[ext_id] != 0 or credits != -1 else 0 # dont let 0 go to -1!!
    else:
        ext_list[ext_id] = credits

    with open(f"{DATA_DIR}/ext.json", "w") as ext_file:
        json.dump(ext_list, ext_file)